#pragma once
#include <stdbool.h>
#include <pthread.h>

struct music_player{
	char filename[100];
	void (*start)(struct music_player*);
	void (*stop)(struct music_player*);
	bool playing;
	pthread_t pid;
	int rate;
	int channels;
	int seconds;
};

void * music_thread_f(void *);	// the thread the music plays on
struct music_player * make_music_player(char _fname[100], int, int, int);
void set_rate_channels_seconds(struct music_player*, int , int , int);
void destroy_music_player(struct music_player *);

void __start (struct music_player*);
void __stop (struct music_player*);



#include "music_player.h"
#include <pthread.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <alsa/asoundlib.h>
#include <sndfile.h>
#include <unistd.h>

// William Doyle

//#define DEBUG
// HUGE help from https://gist.github.com/ghedo/963382/815c98d1ba0eda1b486eb9d80d9a91a81d995283
// additional help from (using a file instead of piping the file): https://stackoverflow.com/questions/27791694/alsa-application-to-read-and-play-a-wav-file-on-raspberry-pi
//help from (see question 8): http://www.mega-nerd.com/libsndfile/FAQ.html

void close_free(snd_pcm_t * pcm_handle){
//		snd_pcm_drain(pcm_handle);
		snd_pcm_close(pcm_handle);
		snd_config_update_free_global();
}

void * play_tune (void * data){
	struct music_player * self = (struct music_player *) data;	
	unsigned short pcm;
	unsigned int tmp;

	int buff_size;
	
	SF_INFO sfinfo;
	SNDFILE * input_file = NULL;
	
	while (self->playing == true){
		short *buff;
		input_file = sf_open(self->filename, SFM_READ, &sfinfo);
		snd_pcm_t * pcm_handle;
		snd_pcm_hw_params_t * params;
		snd_pcm_uframes_t frames;
#define PCM_DEVICE "default"
		
		// open PCM device in playback mode 
		if ( pcm = snd_pcm_open(&pcm_handle, PCM_DEVICE, SND_PCM_STREAM_PLAYBACK, 0) < 0 ){
			fprintf(stderr, "Error in %s, Cannot open PCM device %s!\n", __func__, PCM_DEVICE);
		}
		
		snd_pcm_hw_params_alloca(&params);				// allocate parameters object
		snd_pcm_hw_params_any(pcm_handle, params);		//  fill with default values

		// set parameters
		if ( pcm = snd_pcm_hw_params_set_access(pcm_handle, params, SND_PCM_ACCESS_RW_INTERLEAVED)     < 0 ){
			fprintf(stderr, "Cannot set interleaved mode. %s\n", snd_strerror(pcm));
		}
		if ( pcm = snd_pcm_hw_params_set_format(pcm_handle, params, SND_PCM_FORMAT_S16_LE)  < 0 ){
			fprintf(stderr, " Cannot set format. %s\n", snd_strerror(pcm));
		}
		if ( pcm = snd_pcm_hw_params_set_channels(pcm_handle, params, self->channels) < 0 ){
			fprintf(stderr, " Cannot set channels number. %s\n", snd_strerror(pcm));
		}
		if ( pcm = snd_pcm_hw_params_set_rate_near(pcm_handle, params, &(self->rate), 0) < 0 ){
			fprintf(stderr, " Cannot set rate. %s\n", snd_strerror(pcm));
		}

		// write parameters
		if ( pcm = snd_pcm_hw_params(pcm_handle, params) < 0){
			fprintf(stderr, " Cannot set hardware parameters %s\n", snd_strerror(pcm)); 
		}	

		// Resume information
		snd_pcm_hw_params_get_channels(params, &tmp);
		snd_pcm_hw_params_get_rate(params, &tmp, 0);
		
		/* Allocate buffer to hold single period */
		snd_pcm_hw_params_get_period_size(params, &frames, 0);

		buff_size = frames * self->channels * 2 /* 2 -> sample size */;
		buff = malloc(buff_size);

		snd_pcm_hw_params_get_period_time(params, &tmp, NULL);

		for (int i = (self->seconds * 1000000) / tmp; i > 0; i--) {
			
			if (self->playing == false){
				snd_pcm_close(pcm_handle);
				snd_config_update_free_global();
				if (buff != NULL){
					free(buff);
				}
				snd_pcm_drop(pcm_handle);
		//		if (params != NULL){
		//			snd_pcm_hw_params_free(params);
		//		}
				return data;
			}
			if (pcm = sf_readf_short(input_file, buff, frames) == 0) {
				fprintf(stderr, "Early end of file.\n");
				close_free(pcm_handle);
				if (buff != NULL){
					free(buff);
				}
				snd_pcm_drop(pcm_handle);
				return data;
			}
			if (pcm = snd_pcm_writei(pcm_handle, buff, frames) == -EPIPE) {
				snd_pcm_prepare(pcm_handle);
			} else if (pcm < 0) {
				fprintf(stderr, "ERROR. Can't write to PCM device. %s\n", snd_strerror(pcm));
			}
		}// end for

		snd_pcm_drain(pcm_handle);
		close_free(pcm_handle);

		if (buff != NULL){
			free(buff);
		}

		if (self->playing == false){
			return data;		
		}
	}
	return data;
}

// create new music_player on heap
struct music_player * make_music_player(char _fname[100], int _rate, int _channels, int _seconds){
	struct music_player * self = malloc(sizeof(struct music_player));
	if (self == NULL){
		fprintf(stderr, "Malloc failed in %s. Exiting!\n", __func__);
		exit(EXIT_FAILURE);
	}
	strcpy(self->filename, _fname);
	self->start = __start;
	self->stop = __stop;
	self->playing = false;

	set_rate_channels_seconds(self, _rate, _channels, _seconds);

	return self;
}

// safely free the memory used by the music_player 
void destroy_music_player(struct music_player * self){
	if (self == NULL){
		return;
	}
	if (self->playing == true){
		self->stop(self);
	}
	free(self);
}

// begin playing audio 
void __start (struct music_player * self){
	self->playing = true;
	pthread_t music_thread;
	pthread_create(&music_thread, NULL, play_tune, (void *)self);
}

// cease playing audio
void __stop (struct music_player * self){
	self->playing = false;
	usleep(100000); // allow enough time to pass for thread to know it should end
}

void set_rate_channels_seconds(struct music_player * self, int _rate, int _channels, int _seconds){
	self->rate = _rate;
	self->channels = _channels;
	self->seconds = _seconds;
}

 // PCM		Pulse-code modulation 		https://en.wikipedia.org/wiki/Pulse-code_modulation

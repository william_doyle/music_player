#include "music_player.h"
#include <unistd.h>
#include <stdio.h>



int main (void){
#define _FILE "music/MidAirMachineAStateOfDespair.wav"
#define _RATE		44100 
#define _CHANNELS	2
#define _SECONDS	95
#define SECOND 1000000
	struct music_player * player = make_music_player(_FILE, _RATE, _CHANNELS, _SECONDS);

	player->start(player);

	usleep(SECOND * 6); // let song play about 1.5 times

	player->stop(player);
	printf("stoped\n");

	usleep(SECOND*3);
	destroy_music_player(player);
}
